var validator = {
    kiemTraRong: (value, myDocument, message) => {
        if (value.length == 0) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraDoDai: (value, myDocument, message, min, max) => {
        if (value.length < min || value.length > max) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraEmail: (value, myDocument, message) => {
        const re =
            /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraMatKhau: (value, myDocument, message) => {
        const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
        if (re.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    },
    kiemTraDiemSo: (value, myDocument, message, min, max) => {
        if (value < min || value > max) {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        } else {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        }
    },
    kiemTraTrungID: (svArr, id, myDocument, message) => {
        if (svArr.length) {
            var biTrung = false;
            for (var i = 0; i < svArr.length; i++) {
                item = svArr[i];
                if (item.id == id) {
                    biTrung = true;
                }
            }
            if (biTrung) {
                document.getElementById(myDocument).innerHTML = message;
                return false;
            } else {
                document.getElementById(myDocument).innerHTML = '';
                return true;
            }
        }
    },
    kiemTraTen: (value, myDocument, message) => {
        var regex = /^([^0-9]*)$/;
        if (regex.test(value)) {
            document.getElementById(myDocument).innerHTML = '';
            return true;
        } else {
            document.getElementById(myDocument).innerHTML = message;
            return false;
        }
    }

}