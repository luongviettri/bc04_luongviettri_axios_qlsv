/**
 * Nhận thông tin từ API
 */
const BASE_URL = "https://62db6cb5e56f6d82a77285f3.mockapi.io";
document.getElementById('txtMaSV').disabled = true;
var btnThem = document.getElementById('btn-them');
var btnCapNhat = document.getElementById('btn-cap-nhat');
btnCapNhat.disabled = true;
function laySinhVien() {
    batLoading();
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then((res) => {
            renderDSSV(res.data);
            // ! API lấy về ko có function
            // ! ==> thêm function  = cách gọi cập nhật lại đối tượng --- xử lý trong render

            tatLoading();
        })
        .catch((err) => {
            console.log('err: ', err);
            tatLoading();
        });
}
laySinhVien();
function handleValidName(event) {
    var isValid = true;
    // ! xử lý ID: ko cần
    // !xử lý name
    isValid = validator.kiemTraRong(event.target.value, 'spanTenSV', "Tên ko dc rỗng")
        && validator.kiemTraTen(event.target.value, 'spanTenSV', "Tên ko dc chứa kí tự  số");
}
function handleValidMail(event) {
    var isValid = true;
    //! xử lý email
    isValid =
        validator.kiemTraRong(event.target.value, 'spanEmailSV', "email ko dc rỗng") && validator.kiemTraEmail(event.target.value, 'spanEmailSV', "email ko đúng định dạng");
}
function handleValidPass(event) {
    var isValid = true;

    //! xử lý mật khẩu
    isValid =
        validator.kiemTraRong(event.target.value, 'spanMatKhau', 'mật khẩu ko dc rỗng')
        && validator.kiemTraMatKhau(event.target.value, 'spanMatKhau', `mật khẩu ít nhất 8 kí tự, ít nhất 1 số, 1 chữ cái thường, 1 chữ cái hoa, ko chứa kí tự đặc biệt
  `)
}
function handleValidMath(event) {
    var isValid = true;
    //! xử lý điểm toán
    isValid =
        validator.kiemTraRong(event.target.value, 'spanToan', 'Vui lòng nhập điểm toán')
        && validator.kiemTraDiemSo(event.target.value, 'spanToan', 'Điểm số phải từ 0 đến 10', 0, 10);

}
function handleValidPhys(event) {
    var isValid = true;

    // !xử lý điểm lý
    isValid =
        validator.kiemTraRong(event.target.value, 'spanLy', 'Vui lòng nhập điểm lý')
        && validator.kiemTraDiemSo(event.target.value, 'spanLy', 'Điểm số phải từ 0 đến 10', 0, 10);

}
function handleValidCheMis(event) {
    var isValid = true;

    //! xử lý điểm hóa
    isValid = validator.kiemTraRong(event.target.value, 'spanHoa', 'Vui lòng nhập điểm hóa')
        && validator.kiemTraDiemSo(event.target.value, 'spanHoa', 'Điểm số phải từ 0 đến 10', 0, 10);
}
function themNhanVien() {
    var newSV = layThongTinTuForm();
    var isValid = true;
    // ! xử lý ID: ko cần
    // !xử lý name
    isValid &= validator.kiemTraRong(newSV.name, 'spanTenSV', "Tên ko dc rỗng")
        && validator.kiemTraTen(newSV.name, 'spanTenSV', "Tên ko dc chứa kí tự  số");
    //! xử lý email
    isValid &=
        validator.kiemTraRong(newSV.email, 'spanEmailSV', "email ko dc rỗng") && validator.kiemTraEmail(newSV.email, 'spanEmailSV', "email ko đúng định dạng");
    //! xử lý mật khẩu
    isValid &=
        validator.kiemTraRong(newSV.password, 'spanMatKhau', 'mật khẩu ko dc rỗng')
        && validator.kiemTraMatKhau(newSV.password, 'spanMatKhau', `mật khẩu ít nhất 8 kí tự, ít nhất 1 số, 1 chữ cái thường, 1 chữ cái hoa, ko chứa kí tự đặc biệt
        `)
    //! xử lý điểm toán
    isValid &=
        validator.kiemTraRong(newSV.math, 'spanToan', 'Vui lòng nhập điểm toán')
        && validator.kiemTraDiemSo(newSV.math, 'spanToan', 'Điểm số phải từ 0 đến 10', 0, 10);
    // !xử lý điểm lý
    isValid &=
        validator.kiemTraRong(newSV.physics, 'spanLy', 'Vui lòng nhập điểm lý')
        && validator.kiemTraDiemSo(newSV.physics, 'spanLy', 'Điểm số phải từ 0 đến 10', 0, 10);
    //! xử lý điểm hóa
    isValid &= validator.kiemTraRong(newSV.chemistry, 'spanHoa', 'Vui lòng nhập điểm hóa')
        && validator.kiemTraDiemSo(newSV.chemistry, 'spanHoa', 'Điểm số phải từ 0 đến 10', 0, 10);
    if (!isValid) {
        return;
    }
    batLoading();
    axios({
        url: `${BASE_URL}/sv/`,
        method: "POST",
        data: newSV
    })
        .then((res) => {
            laySinhVien();
            handleReset();
        })
        .catch((err) => {
            console.log('err: ', err);
        })
}
function xoaSinhVien(id) {
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "DELETE",
    })
        .then((res) => {
            laySinhVien();
        })
        .catch((err) => {
            console.log(err);
        });
}
// !hàm lấy đối tượng sinh viên được click vào
function suaSinhVien(id) {
    btnThem.disabled = true;
    btnCapNhat.disabled = false;
    axios({
        url: `${BASE_URL}/sv/${id}`,
        method: "GET",
    }).then((res) => {
        // ! Hiện thông tin của object lên form
        var nhanVien = res.data;
        hienThongTinLenForm(nhanVien);
        //! ẩn trường mã sinh viên
        document.getElementById('txtMaSV').disabled = true;
    }).catch((err) => {
        console.log("err>>>", err);
    })
}
// ! hàm này để gắn vào nút cập nhật nút cập nhật
function capNhatSinhVien() {
    var newSV = layThongTinTuForm();
    if (!newSV.id) {
        alert("Không có gì để cập nhật");
        return;
    }
    //! validate
    var isValid = true;
    // xử lý name
    // dấu & cho phép code đi tiếp kể cả khi isValid ban đầu là false
    isValid = isValid & validator.kiemTraRong(newSV.name, 'spanTenSV', "Tên ko dc rỗng")
    // xử lý email
    isValid &=
        validator.kiemTraRong(newSV.email, 'spanEmailSV', "email ko dc rỗng") && validator.kiemTraEmail(newSV.email, 'spanEmailSV', "email ko đúng định dạng");
    // xử lý mật khẩu
    isValid &=
        validator.kiemTraRong(newSV.password, 'spanMatKhau', 'mật khẩu ko dc rỗng')
        && validator.kiemTraMatKhau(newSV.password, 'spanMatKhau', `mật khẩu ít nhất 8 kí tự, ít nhất 1 số, 1 chữ cái thường, 1 chữ cái hoa, ko chứa kí tự đặc biệt
          `)
    // xử lý điểm toán
    isValid &=
        validator.kiemTraRong(newSV.math, 'spanToan', 'Vui lòng nhập điểm toán')
        && validator.kiemTraDiemSo(newSV.math, 'spanToan', 'Điểm số phải từ 0 đến 10', 0, 10);
    // xử lý điểm lý
    isValid &=
        validator.kiemTraRong(newSV.physics, 'spanLy', 'Vui lòng nhập điểm lý')
        && validator.kiemTraDiemSo(newSV.physics, 'spanLy', 'Điểm số phải từ 0 đến 10', 0, 10);
    // xử lý điểm hóa
    isValid &=
        validator.kiemTraRong(newSV.chemistry, 'spanHoa', 'Vui lòng nhập điểm hóa')
        && validator.kiemTraDiemSo(newSV.chemistry, 'spanHoa', 'Điểm số phải từ 0 đến 10', 0, 10);

    if (!isValid) {
        return;
    }
    btnCapNhat.disabled = true;
    btnThem.disabled = false;
    axios({
        url: `${BASE_URL}/sv/${newSV.id}`,
        method: "PUT",
        data: {
            name: newSV.name,
            email: newSV.email,
            password: newSV.password,
            math: newSV.math,
            physics: newSV.physics,
            chemistry: newSV.chemistry
        }
    }).then((res) => {
        laySinhVien();
        handleReset();
    }).catch((err) => {
        console.log(err);
    })
}
function resetAll() {
    document.getElementById('txtMaSV').value = '';
    document.getElementById('txtTenSV').value = '';
    document.getElementById('txtEmail').value = '';
    document.getElementById('txtPass').value = '';
    document.getElementById('txtDiemToan').value = '';
    document.getElementById('txtDiemLy').value = '';
    document.getElementById('txtDiemHoa').value = '';
    //
    document.getElementById('spanMaSV').innerText = '';
    document.getElementById('spanTenSV').innerText = '';
    document.getElementById('spanEmailSV').innerText = '';
    document.getElementById('spanMatKhau').innerText = '';
    document.getElementById('spanToan').innerText = '';
    document.getElementById('spanLy').innerText = '';
    document.getElementById('spanHoa').innerText = '';
    //
    document.getElementById("txtSearch").value = '';

    var btnThem = document.getElementById('btn-them');
    btnThem.disabled = false;
    btnCapNhat.disabled = true;

    laySinhVien();

}
function timKiemSinhVien() {
    console.log('ok');
    // ! lấy tên cần tìm
    var name = document.getElementById('txtSearch').value;
    // console.log('name: ', name);
    // ! gọi API và tìm tên 
    axios({
        url: `${BASE_URL}/sv`,
        method: "GET",
    })
        .then((res) => {
            var sinhVienCanTim = [];
            var allSinhVien = res.data;
            allSinhVien.forEach(sinhVien => {
                if (sinhVien.name.toLowerCase().includes(name.toLowerCase())) {
                    sinhVienCanTim.push(sinhVien);
                }
            });
            renderDSSV(sinhVienCanTim);
        })
        .catch((err) => {
            console.log('err: ', err);
        });

}

