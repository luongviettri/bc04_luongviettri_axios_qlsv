renderDSSV = (dssv) => {
    var contentHTML = '';
    dssv.forEach(sv => {
       
        var sinhVienDuocLayVe = sv;
        sv = new SinhVien(
            sinhVienDuocLayVe.id,
            sinhVienDuocLayVe.name,
            sinhVienDuocLayVe.email,
            sinhVienDuocLayVe.password,
            sinhVienDuocLayVe.math,
            sinhVienDuocLayVe.physics,
            sinhVienDuocLayVe.chemistry
        )
        var contentTr =
            `
        <tr>
            <td>${sv.id}</td>
            <td>${sv.name}</td>
            <td>${sv.email}</td>
            <td>${sv.calculateAverage()}</td>
            <td>
            <button 
            onclick=suaSinhVien('${sv.id}')
            class = "btn btn-warning" >Sửa </button>
            <button onclick=xoaSinhVien('${sv.id}')  class = "btn btn-danger" >Xóa </button>
            </td>
        </tr>
        `
        contentHTML += contentTr;
    });
    document.getElementById('tbodySinhVien').innerHTML = contentHTML;
}
function batLoading() {
    document.getElementById('loading').style.display = "flex";
}
function tatLoading() {
    document.getElementById('loading').style.display = "none";
}
function hienThongTinLenForm(nhanVien) {
    document.getElementById('txtMaSV').value = nhanVien.id;
    document.getElementById('txtTenSV').value = nhanVien.name;
    document.getElementById('txtEmail').value = nhanVien.email;
    document.getElementById('txtPass').value = nhanVien.password;
    document.getElementById('txtDiemToan').value = nhanVien.math;
    document.getElementById('txtDiemLy').value = nhanVien.physics;
    document.getElementById('txtDiemHoa').value = nhanVien.chemistry;
}
function layThongTinTuForm() {
    const id = document.getElementById('txtMaSV').value;
    const name = document.getElementById('txtTenSV').value;
    const email = document.getElementById('txtEmail').value;
    const password = document.getElementById('txtPass').value;
    const math = document.getElementById('txtDiemToan').value;
    const physics = document.getElementById('txtDiemLy').value;
    const chemistry = document.getElementById('txtDiemHoa').value;
    return new SinhVien(id, name, email, password, math, physics, chemistry);
}
function handleReset() {
    document.getElementById('txtMaSV').value = '';
    document.getElementById('txtTenSV').value = '';
    document.getElementById('txtEmail').value = '';
    document.getElementById('txtPass').value = '';
    document.getElementById('txtDiemToan').value = '';
    document.getElementById('txtDiemLy').value = '';
    document.getElementById('txtDiemHoa').value = '';
}
